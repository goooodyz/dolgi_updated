﻿using Dolgi_Updated.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dolgi_Updated
{
    class DB_communicate : IDB_communicate
    {
        public string GetData(string id)
        {
            string ret="";
            DB db = new DB();
            db.OpenConnection();

            MySqlCommand command = new MySqlCommand("SELECT `Pavel` FROM `users` WHERE `id` = @Id", db.GetConnection());
            command.Parameters.Add("@Id", MySqlDbType.VarChar).Value = id;
            ret = command.ExecuteScalar().ToString()+"@";

            command = new MySqlCommand("SELECT `Makara` FROM `users` WHERE `id` = @Id", db.GetConnection());
            command.Parameters.Add("@Id", MySqlDbType.VarChar).Value = id;
            ret += command.ExecuteScalar().ToString() + "@";

            command = new MySqlCommand("SELECT `Kisil` FROM `users` WHERE `id` = @Id", db.GetConnection());
            command.Parameters.Add("@Id", MySqlDbType.VarChar).Value = id;
            ret += command.ExecuteScalar().ToString() + "@";

            command = new MySqlCommand("SELECT `Vlad` FROM `users` WHERE `id` = @Id", db.GetConnection());
            command.Parameters.Add("@Id", MySqlDbType.VarChar).Value = id;
            ret += command.ExecuteScalar().ToString() + "@";

            db.CloseConnection();


            return  ret;
        }

        public void SetData(double[] positiveDept, double[] negativeDept, string id)
        {
            DB db = new DB();
            db.OpenConnection();
            MySqlCommand command = new MySqlCommand("UPDATE `users` SET `Pavel`=@zam1, `Makara`=@zam2, `Kisil`=@zam3, `Vlad`=@zam4 WHERE `id` = @Id", db.GetConnection());
            command.Parameters.Add("@Id", MySqlDbType.VarChar).Value = id;

            command.Parameters.Add("@zam1", MySqlDbType.Double).Value = positiveDept[0];
            command.Parameters.Add("@zam2", MySqlDbType.Double).Value = positiveDept[1];
            command.Parameters.Add("@zam3", MySqlDbType.Double).Value = positiveDept[2];
            command.Parameters.Add("@zam4", MySqlDbType.Double).Value = positiveDept[3];
            command.ExecuteNonQuery();

            if (id == "3")
            {
                command = new MySqlCommand("UPDATE `users` SET `Pavel`=@zam  WHERE `id` =@Name ", db.GetConnection());
                command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = 6;
                command.Parameters.Add("@zam", MySqlDbType.Double).Value = negativeDept[1];
                command.ExecuteNonQuery();

                command = new MySqlCommand("UPDATE `users` SET `Pavel`=@zam  WHERE `id` =@Name ", db.GetConnection());
                command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = 4;
                command.Parameters.Add("@zam", MySqlDbType.Double).Value = negativeDept[2];
                command.ExecuteNonQuery();

                command = new MySqlCommand("UPDATE `users` SET `Pavel`=@zam  WHERE `id` =@Name ", db.GetConnection());
                command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = 7;
                command.Parameters.Add("@zam", MySqlDbType.Double).Value = negativeDept[3];
                command.ExecuteNonQuery();
            }
            else if (id == "6")
            {
                command = new MySqlCommand("UPDATE `users` SET `Makara`=@zam  WHERE `id` =@Name ", db.GetConnection());
                command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = 3;
                command.Parameters.Add("@zam", MySqlDbType.Double).Value = negativeDept[0];
                command.ExecuteNonQuery();

                command = new MySqlCommand("UPDATE `users` SET `Makara`=@zam  WHERE `id` =@Name ", db.GetConnection());
                command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = 4;
                command.Parameters.Add("@zam", MySqlDbType.Double).Value = negativeDept[2];
                command.ExecuteNonQuery();

                command = new MySqlCommand("UPDATE `users` SET `Makara`=@zam  WHERE `id` =@Name ", db.GetConnection());
                command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = 7;
                command.Parameters.Add("@zam", MySqlDbType.Double).Value = negativeDept[3];
                command.ExecuteNonQuery();
            }
            else if(id == "4")
            {
                command = new MySqlCommand("UPDATE `users` SET `Kisil`=@zam  WHERE `id` =@Name ", db.GetConnection());
                command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = 3;
                command.Parameters.Add("@zam", MySqlDbType.Double).Value = negativeDept[0];
                command.ExecuteNonQuery();

                command = new MySqlCommand("UPDATE `users` SET `Kisil`=@zam  WHERE `id` =@Name ", db.GetConnection());
                command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = 6;
                command.Parameters.Add("@zam", MySqlDbType.Double).Value = negativeDept[1];
                command.ExecuteNonQuery();

                command = new MySqlCommand("UPDATE `users` SET `Kisil`=@zam  WHERE `id` =@Name ", db.GetConnection());
                command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = 7;
                command.Parameters.Add("@zam", MySqlDbType.Double).Value = negativeDept[3];
                command.ExecuteNonQuery();
            }
            else if( id == "7")
            {
                command = new MySqlCommand("UPDATE `users` SET `Vlad`=@zam  WHERE `id` =@Name ", db.GetConnection());
                command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = 3;
                command.Parameters.Add("@zam", MySqlDbType.Double).Value = negativeDept[0];
                command.ExecuteNonQuery();

                command = new MySqlCommand("UPDATE `users` SET `Vlad`=@zam  WHERE `id` =@Name ", db.GetConnection());
                command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = 6;
                command.Parameters.Add("@zam", MySqlDbType.Double).Value = negativeDept[1];
                command.ExecuteNonQuery();

                command = new MySqlCommand("UPDATE `users` SET `Vlad`=@zam  WHERE `id` =@Name ", db.GetConnection());
                command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = 4;
                command.Parameters.Add("@zam", MySqlDbType.Double).Value = negativeDept[2];
                command.ExecuteNonQuery();
            }

            db.CloseConnection();
        }
    }
}
