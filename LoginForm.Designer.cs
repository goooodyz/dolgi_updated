﻿namespace Dolgi_Updated
{
    partial class LoginForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.LbToMove = new System.Windows.Forms.Label();
            this.BtClose = new System.Windows.Forms.Button();
            this.TbLogin = new System.Windows.Forms.TextBox();
            this.TbPass = new System.Windows.Forms.TextBox();
            this.BtLogin = new System.Windows.Forms.Button();
            this.LbLoginPls = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LbToMove
            // 
            this.LbToMove.BackColor = System.Drawing.SystemColors.Control;
            this.LbToMove.Dock = System.Windows.Forms.DockStyle.Top;
            this.LbToMove.Location = new System.Drawing.Point(0, 0);
            this.LbToMove.Name = "LbToMove";
            this.LbToMove.Size = new System.Drawing.Size(616, 29);
            this.LbToMove.TabIndex = 0;
            this.LbToMove.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LbToMove_MouseDown);
            this.LbToMove.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbToMove_MouseMove);
            // 
            // BtClose
            // 
            this.BtClose.BackColor = System.Drawing.SystemColors.Control;
            this.BtClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BtClose.Location = new System.Drawing.Point(587, 0);
            this.BtClose.Name = "BtClose";
            this.BtClose.Size = new System.Drawing.Size(29, 29);
            this.BtClose.TabIndex = 1;
            this.BtClose.Text = "X";
            this.BtClose.UseVisualStyleBackColor = false;
            this.BtClose.Click += new System.EventHandler(this.BtClose_Click);
            // 
            // TbLogin
            // 
            this.TbLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TbLogin.ForeColor = System.Drawing.SystemColors.GrayText;
            this.TbLogin.Location = new System.Drawing.Point(12, 152);
            this.TbLogin.Name = "TbLogin";
            this.TbLogin.Size = new System.Drawing.Size(592, 41);
            this.TbLogin.TabIndex = 2;
            this.TbLogin.Text = "Login";
            this.TbLogin.Enter += new System.EventHandler(this.TbLogin_Enter);
            this.TbLogin.Leave += new System.EventHandler(this.TbLogin_Leave);
            // 
            // TbPass
            // 
            this.TbPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TbPass.ForeColor = System.Drawing.SystemColors.GrayText;
            this.TbPass.Location = new System.Drawing.Point(12, 225);
            this.TbPass.Name = "TbPass";
            this.TbPass.Size = new System.Drawing.Size(592, 41);
            this.TbPass.TabIndex = 3;
            this.TbPass.Text = "Password";
            this.TbPass.Enter += new System.EventHandler(this.TbPass_Enter);
            this.TbPass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbPass_KeyDown);
            this.TbPass.Leave += new System.EventHandler(this.TbPass_Leave);
            // 
            // BtLogin
            // 
            this.BtLogin.AutoSize = true;
            this.BtLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BtLogin.Location = new System.Drawing.Point(234, 286);
            this.BtLogin.Name = "BtLogin";
            this.BtLogin.Size = new System.Drawing.Size(138, 46);
            this.BtLogin.TabIndex = 4;
            this.BtLogin.Text = "Login";
            this.BtLogin.UseVisualStyleBackColor = true;
            this.BtLogin.Click += new System.EventHandler(this.BtLogin_Click);
            // 
            // LbLoginPls
            // 
            this.LbLoginPls.AutoSize = true;
            this.LbLoginPls.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LbLoginPls.Location = new System.Drawing.Point(110, 82);
            this.LbLoginPls.Name = "LbLoginPls";
            this.LbLoginPls.Size = new System.Drawing.Size(397, 36);
            this.LbLoginPls.TabIndex = 5;
            this.LbLoginPls.Text = "Login please in your account";
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(616, 459);
            this.Controls.Add(this.LbLoginPls);
            this.Controls.Add(this.BtLogin);
            this.Controls.Add(this.TbPass);
            this.Controls.Add(this.TbLogin);
            this.Controls.Add(this.BtClose);
            this.Controls.Add(this.LbToMove);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Dolgi";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LbToMove;
        private System.Windows.Forms.Button BtClose;
        private System.Windows.Forms.TextBox TbLogin;
        private System.Windows.Forms.TextBox TbPass;
        private System.Windows.Forms.Button BtLogin;
        private System.Windows.Forms.Label LbLoginPls;
    }
}

