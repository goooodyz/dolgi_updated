﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dolgi_Updated
{
    public partial class LoginForm : Form
    {
        public LoginForm(int left, int top)
        {
            InitializeComponent();
            this.Left = left;
            this.Top = top;
        }

        private void BtClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        Point lastPoint;

        private void LbToMove_MouseMove(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void LbToMove_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {

        }

        private void TbLogin_Enter(object sender, EventArgs e)
        {
            if (TbLogin.Text == "Login")
            {
                TbLogin.ForeColor = Color.Black;
                TbLogin.Clear();
            }
        }

        private void TbLogin_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TbLogin.Text))
            {
                TbLogin.ForeColor = Color.Gray;
                TbLogin.Text = "Login";
            }
        }

        private void TbPass_Enter(object sender, EventArgs e)
        {
            if (TbPass.Text == "Password")
            {
                TbPass.ForeColor = Color.Black;
                TbPass.Clear();
                TbPass.UseSystemPasswordChar = true;
            }
        }

        private void TbPass_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TbPass.Text))
            {
                TbPass.UseSystemPasswordChar = false;
                TbPass.ForeColor = Color.Gray;
                TbPass.Text = "Password";
            }
        }

        private void TbPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                BtLogin_Click(null, null);
            }
        }
        private void BtLogin_Click(object sender, EventArgs e)
        {

            if (TbLogin.Text == "Login")
            {
                MessageBox.Show("Enter login");
                return;
            }else if (TbPass.Text == "Password")
            {
                MessageBox.Show("Enter password");
                return;
            }

            DB db = new DB();

            DataTable table = new DataTable();
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            MySqlCommand command = new MySqlCommand("SELECT * FROM `users` WHERE `name`=@Log and `pass`=@Pas",db.GetConnection());
            
            command.Parameters.Add("@Log", MySqlDbType.VarChar).Value=TbLogin.Text;
            command.Parameters.Add("@Pas", MySqlDbType.VarChar).Value = TbPass.Text;

            adapter.SelectCommand = command;

            adapter.Fill(table);


            if(table.Rows.Count > 0)
            {
                db.OpenConnection();
                command = new MySqlCommand("SELECT `id` FROM `users` WHERE `name`=@Log and `pass`=@Pas", db.GetConnection());
                command.Parameters.Add("@Log", MySqlDbType.VarChar).Value = TbLogin.Text;
                command.Parameters.Add("@Pas", MySqlDbType.VarChar).Value = TbPass.Text;
                string id = command.ExecuteScalar().ToString();

                db.CloseConnection();
                this.Hide();
                MainForm openForm = new MainForm(this.Left,this.Top,id);
                openForm.Show();
            }
            else
            {
                MessageBox.Show("Wrong password or login");
                TbPass.Clear();
            }

            
        }


    }
}
