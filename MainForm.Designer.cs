﻿namespace Dolgi_Updated
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtClose = new System.Windows.Forms.Button();
            this.LbToMove = new System.Windows.Forms.Label();
            this.Lbpavel = new System.Windows.Forms.Label();
            this.LbMakara = new System.Windows.Forms.Label();
            this.LbKisil = new System.Windows.Forms.Label();
            this.LbVlad = new System.Windows.Forms.Label();
            this.LbPavelDolg = new System.Windows.Forms.Label();
            this.LbMakaraDolg = new System.Windows.Forms.Label();
            this.LbKisilDolg = new System.Windows.Forms.Label();
            this.LbVladDolg = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TbValue = new System.Windows.Forms.TextBox();
            this.BtCalculate = new System.Windows.Forms.Button();
            this.CBPavel = new System.Windows.Forms.CheckBox();
            this.CBMakara = new System.Windows.Forms.CheckBox();
            this.CBKisil = new System.Windows.Forms.CheckBox();
            this.CBVlad = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TbPavel = new System.Windows.Forms.TextBox();
            this.TbMakara = new System.Windows.Forms.TextBox();
            this.TbKisil = new System.Windows.Forms.TextBox();
            this.TbVlad = new System.Windows.Forms.TextBox();
            this.BtBAck = new System.Windows.Forms.Button();
            this.CBSelectAll = new System.Windows.Forms.CheckBox();
            this.CbCalc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtClose
            // 
            this.BtClose.BackColor = System.Drawing.SystemColors.Control;
            this.BtClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BtClose.Location = new System.Drawing.Point(587, 0);
            this.BtClose.Name = "BtClose";
            this.BtClose.Size = new System.Drawing.Size(29, 29);
            this.BtClose.TabIndex = 3;
            this.BtClose.Text = "X";
            this.BtClose.UseVisualStyleBackColor = false;
            this.BtClose.Click += new System.EventHandler(this.BtClose_Click);
            // 
            // LbToMove
            // 
            this.LbToMove.BackColor = System.Drawing.SystemColors.Control;
            this.LbToMove.Dock = System.Windows.Forms.DockStyle.Top;
            this.LbToMove.Location = new System.Drawing.Point(0, 0);
            this.LbToMove.Name = "LbToMove";
            this.LbToMove.Size = new System.Drawing.Size(616, 29);
            this.LbToMove.TabIndex = 2;
            this.LbToMove.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LbToMove_MouseDown);
            this.LbToMove.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LbToMove_MouseMove);
            // 
            // Lbpavel
            // 
            this.Lbpavel.AutoSize = true;
            this.Lbpavel.BackColor = System.Drawing.SystemColors.HotTrack;
            this.Lbpavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Lbpavel.Location = new System.Drawing.Point(371, 38);
            this.Lbpavel.Name = "Lbpavel";
            this.Lbpavel.Size = new System.Drawing.Size(61, 25);
            this.Lbpavel.TabIndex = 4;
            this.Lbpavel.Text = "Pavel";
            // 
            // LbMakara
            // 
            this.LbMakara.AutoSize = true;
            this.LbMakara.BackColor = System.Drawing.SystemColors.HotTrack;
            this.LbMakara.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LbMakara.Location = new System.Drawing.Point(371, 74);
            this.LbMakara.Name = "LbMakara";
            this.LbMakara.Size = new System.Drawing.Size(78, 25);
            this.LbMakara.TabIndex = 5;
            this.LbMakara.Text = "Makara";
            // 
            // LbKisil
            // 
            this.LbKisil.AutoSize = true;
            this.LbKisil.BackColor = System.Drawing.SystemColors.HotTrack;
            this.LbKisil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LbKisil.Location = new System.Drawing.Point(371, 112);
            this.LbKisil.Name = "LbKisil";
            this.LbKisil.Size = new System.Drawing.Size(48, 25);
            this.LbKisil.TabIndex = 6;
            this.LbKisil.Text = "Kisil";
            // 
            // LbVlad
            // 
            this.LbVlad.AutoSize = true;
            this.LbVlad.BackColor = System.Drawing.SystemColors.HotTrack;
            this.LbVlad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LbVlad.Location = new System.Drawing.Point(371, 151);
            this.LbVlad.Name = "LbVlad";
            this.LbVlad.Size = new System.Drawing.Size(52, 25);
            this.LbVlad.TabIndex = 7;
            this.LbVlad.Text = "Vlad";
            // 
            // LbPavelDolg
            // 
            this.LbPavelDolg.BackColor = System.Drawing.SystemColors.Control;
            this.LbPavelDolg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LbPavelDolg.Location = new System.Drawing.Point(450, 38);
            this.LbPavelDolg.Name = "LbPavelDolg";
            this.LbPavelDolg.Size = new System.Drawing.Size(154, 23);
            this.LbPavelDolg.TabIndex = 8;
            this.LbPavelDolg.Text = "   ";
            // 
            // LbMakaraDolg
            // 
            this.LbMakaraDolg.BackColor = System.Drawing.SystemColors.Control;
            this.LbMakaraDolg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LbMakaraDolg.Location = new System.Drawing.Point(450, 74);
            this.LbMakaraDolg.Name = "LbMakaraDolg";
            this.LbMakaraDolg.Size = new System.Drawing.Size(154, 23);
            this.LbMakaraDolg.TabIndex = 9;
            this.LbMakaraDolg.Text = "   ";
            // 
            // LbKisilDolg
            // 
            this.LbKisilDolg.BackColor = System.Drawing.SystemColors.Control;
            this.LbKisilDolg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LbKisilDolg.Location = new System.Drawing.Point(450, 114);
            this.LbKisilDolg.Name = "LbKisilDolg";
            this.LbKisilDolg.Size = new System.Drawing.Size(154, 23);
            this.LbKisilDolg.TabIndex = 10;
            this.LbKisilDolg.Text = "   ";
            // 
            // LbVladDolg
            // 
            this.LbVladDolg.BackColor = System.Drawing.SystemColors.Control;
            this.LbVladDolg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LbVladDolg.Location = new System.Drawing.Point(450, 153);
            this.LbVladDolg.Name = "LbVladDolg";
            this.LbVladDolg.Size = new System.Drawing.Size(154, 23);
            this.LbVladDolg.TabIndex = 11;
            this.LbVladDolg.Text = "   ";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(364, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1, 430);
            this.label1.TabIndex = 12;
            // 
            // TbValue
            // 
            this.TbValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TbValue.Location = new System.Drawing.Point(13, 40);
            this.TbValue.Name = "TbValue";
            this.TbValue.Size = new System.Drawing.Size(313, 30);
            this.TbValue.TabIndex = 13;
            this.TbValue.Text = "0";
            this.TbValue.Enter += new System.EventHandler(this.TbValue_Enter);
            this.TbValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbValue_KeyPress);
            this.TbValue.Leave += new System.EventHandler(this.TbValue_Leave);
            // 
            // BtCalculate
            // 
            this.BtCalculate.AutoSize = true;
            this.BtCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BtCalculate.Location = new System.Drawing.Point(13, 412);
            this.BtCalculate.Name = "BtCalculate";
            this.BtCalculate.Size = new System.Drawing.Size(104, 35);
            this.BtCalculate.TabIndex = 14;
            this.BtCalculate.Text = "Calculate";
            this.BtCalculate.UseVisualStyleBackColor = true;
            this.BtCalculate.Click += new System.EventHandler(this.BtCalculate_Click);
            // 
            // CBPavel
            // 
            this.CBPavel.AutoSize = true;
            this.CBPavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CBPavel.Location = new System.Drawing.Point(13, 108);
            this.CBPavel.Name = "CBPavel";
            this.CBPavel.Size = new System.Drawing.Size(83, 29);
            this.CBPavel.TabIndex = 15;
            this.CBPavel.Text = "Pavel";
            this.CBPavel.UseVisualStyleBackColor = true;
            // 
            // CBMakara
            // 
            this.CBMakara.AutoSize = true;
            this.CBMakara.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CBMakara.Location = new System.Drawing.Point(13, 151);
            this.CBMakara.Name = "CBMakara";
            this.CBMakara.Size = new System.Drawing.Size(100, 29);
            this.CBMakara.TabIndex = 16;
            this.CBMakara.Text = "Makara";
            this.CBMakara.UseVisualStyleBackColor = true;
            // 
            // CBKisil
            // 
            this.CBKisil.AutoSize = true;
            this.CBKisil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CBKisil.Location = new System.Drawing.Point(13, 196);
            this.CBKisil.Name = "CBKisil";
            this.CBKisil.Size = new System.Drawing.Size(70, 29);
            this.CBKisil.TabIndex = 17;
            this.CBKisil.Text = "Kisil";
            this.CBKisil.UseVisualStyleBackColor = true;
            // 
            // CBVlad
            // 
            this.CBVlad.AutoSize = true;
            this.CBVlad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CBVlad.Location = new System.Drawing.Point(13, 240);
            this.CBVlad.Name = "CBVlad";
            this.CBVlad.Size = new System.Drawing.Size(74, 29);
            this.CBVlad.TabIndex = 18;
            this.CBVlad.Text = "Vlad";
            this.CBVlad.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(185, 25);
            this.label2.TabIndex = 19;
            this.label2.Text = "use , it\'s important!!!";
            // 
            // TbPavel
            // 
            this.TbPavel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TbPavel.Location = new System.Drawing.Point(131, 107);
            this.TbPavel.Name = "TbPavel";
            this.TbPavel.Size = new System.Drawing.Size(195, 30);
            this.TbPavel.TabIndex = 20;
            this.TbPavel.Text = "0";
            this.TbPavel.Enter += new System.EventHandler(this.TbPavel_Enter);
            this.TbPavel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbPavel_KeyPress);
            this.TbPavel.Leave += new System.EventHandler(this.TbPavel_Leave);
            // 
            // TbMakara
            // 
            this.TbMakara.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TbMakara.Location = new System.Drawing.Point(131, 148);
            this.TbMakara.Name = "TbMakara";
            this.TbMakara.Size = new System.Drawing.Size(195, 30);
            this.TbMakara.TabIndex = 21;
            this.TbMakara.Text = "0";
            this.TbMakara.Enter += new System.EventHandler(this.TbMakara_Enter);
            this.TbMakara.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbMakara_KeyPress);
            this.TbMakara.Leave += new System.EventHandler(this.TbMakara_Leave);
            // 
            // TbKisil
            // 
            this.TbKisil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TbKisil.Location = new System.Drawing.Point(131, 196);
            this.TbKisil.Name = "TbKisil";
            this.TbKisil.Size = new System.Drawing.Size(195, 30);
            this.TbKisil.TabIndex = 22;
            this.TbKisil.Text = "0";
            this.TbKisil.Enter += new System.EventHandler(this.TbKisil_Enter);
            this.TbKisil.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbKisil_KeyPress);
            this.TbKisil.Leave += new System.EventHandler(this.TbKisil_Leave);
            // 
            // TbVlad
            // 
            this.TbVlad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TbVlad.Location = new System.Drawing.Point(131, 239);
            this.TbVlad.Name = "TbVlad";
            this.TbVlad.Size = new System.Drawing.Size(195, 30);
            this.TbVlad.TabIndex = 23;
            this.TbVlad.Text = "0";
            this.TbVlad.Enter += new System.EventHandler(this.TbVlad_Enter);
            this.TbVlad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbVlad_KeyPress);
            this.TbVlad.Leave += new System.EventHandler(this.TbVlad_Leave);
            // 
            // BtBAck
            // 
            this.BtBAck.AutoSize = true;
            this.BtBAck.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BtBAck.Location = new System.Drawing.Point(254, 412);
            this.BtBAck.Name = "BtBAck";
            this.BtBAck.Size = new System.Drawing.Size(104, 35);
            this.BtBAck.TabIndex = 24;
            this.BtBAck.Text = "Back";
            this.BtBAck.UseVisualStyleBackColor = true;
            this.BtBAck.Click += new System.EventHandler(this.BtBAck_Click);
            // 
            // CBSelectAll
            // 
            this.CBSelectAll.AutoSize = true;
            this.CBSelectAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CBSelectAll.Location = new System.Drawing.Point(9, 293);
            this.CBSelectAll.Name = "CBSelectAll";
            this.CBSelectAll.Size = new System.Drawing.Size(113, 29);
            this.CBSelectAll.TabIndex = 25;
            this.CBSelectAll.Text = "Select all";
            this.CBSelectAll.UseVisualStyleBackColor = true;
            this.CBSelectAll.CheckStateChanged += new System.EventHandler(this.CBSelectAll_CheckStateChanged);
            // 
            // CbCalc
            // 
            this.CbCalc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CbCalc.Location = new System.Drawing.Point(477, 412);
            this.CbCalc.Name = "CbCalc";
            this.CbCalc.Size = new System.Drawing.Size(127, 35);
            this.CbCalc.TabIndex = 28;
            this.CbCalc.Text = "calculator";
            this.CbCalc.UseVisualStyleBackColor = true;
            this.CbCalc.Click += new System.EventHandler(this.CbCalc_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(616, 459);
            this.Controls.Add(this.CbCalc);
            this.Controls.Add(this.CBSelectAll);
            this.Controls.Add(this.BtBAck);
            this.Controls.Add(this.TbVlad);
            this.Controls.Add(this.TbKisil);
            this.Controls.Add(this.TbMakara);
            this.Controls.Add(this.TbPavel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CBVlad);
            this.Controls.Add(this.CBKisil);
            this.Controls.Add(this.CBMakara);
            this.Controls.Add(this.CBPavel);
            this.Controls.Add(this.BtCalculate);
            this.Controls.Add(this.TbValue);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LbVladDolg);
            this.Controls.Add(this.LbKisilDolg);
            this.Controls.Add(this.LbMakaraDolg);
            this.Controls.Add(this.LbPavelDolg);
            this.Controls.Add(this.LbVlad);
            this.Controls.Add(this.LbKisil);
            this.Controls.Add(this.LbMakara);
            this.Controls.Add(this.Lbpavel);
            this.Controls.Add(this.BtClose);
            this.Controls.Add(this.LbToMove);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtClose;
        private System.Windows.Forms.Label LbToMove;
        private System.Windows.Forms.Label Lbpavel;
        private System.Windows.Forms.Label LbMakara;
        private System.Windows.Forms.Label LbKisil;
        private System.Windows.Forms.Label LbVlad;
        private System.Windows.Forms.Label LbPavelDolg;
        private System.Windows.Forms.Label LbMakaraDolg;
        private System.Windows.Forms.Label LbKisilDolg;
        private System.Windows.Forms.Label LbVladDolg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TbValue;
        private System.Windows.Forms.Button BtCalculate;
        private System.Windows.Forms.CheckBox CBPavel;
        private System.Windows.Forms.CheckBox CBMakara;
        private System.Windows.Forms.CheckBox CBKisil;
        private System.Windows.Forms.CheckBox CBVlad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TbPavel;
        private System.Windows.Forms.TextBox TbMakara;
        private System.Windows.Forms.TextBox TbKisil;
        private System.Windows.Forms.TextBox TbVlad;
        private System.Windows.Forms.Button BtBAck;
        private System.Windows.Forms.CheckBox CBSelectAll;
        private System.Windows.Forms.Button CbCalc;
    }
}