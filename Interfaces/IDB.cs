﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dolgi_Updated.Interfaces
{
    interface IDB
    {
        void OpenConnection();
        void CloseConnection();
        MySqlConnection GetConnection();
    }
}
