﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dolgi_Updated.Interfaces
{
    public interface IDB_communicate
    {
        string GetData(string id);

        void SetData(double[] positiveDept, double[] negativeDept,string id);
    }
}
