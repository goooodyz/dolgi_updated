﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dolgi_Updated
{
    public partial class MainForm : Form
    {
        private readonly string id;
        public MainForm(int left, int top, string id)
        {
            InitializeComponent();
            this.Left = left;
            this.Top = top;
            this.id = id;
        }

        private void BtClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        Point lastPoint;
        private void LbToMove_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void LbToMove_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void BtBAck_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            DB_communicate dB_Communicate = new DB_communicate();
            if (this.id == "3")
            {
                TbPavel.ReadOnly = true;
            }else if(this.id == "4")
            {
                TbKisil.ReadOnly = true;
            }else if (this.id == "6")
            {
                TbMakara.ReadOnly = true;
            }else if (this.id == "7")
            {
                TbVlad.ReadOnly = true;
            }
            string[] splitted = dB_Communicate.GetData(this.id).Split(new char[] { '@' });
            LbPavelDolg.Text = splitted[0];
            LbMakaraDolg.Text = splitted[1];
            LbKisilDolg.Text = splitted[2];
            LbVladDolg.Text = splitted[3];
        }

        private void BtCalculate_Click(object sender, EventArgs e)
        {
            int toDivision = 0;
            if (CBPavel.Checked)
            {
                toDivision++;
            }
            if (CBMakara.Checked)
            {
                toDivision++;
            }
            if (CBKisil.Checked)
            {
                toDivision++;
            }
            if (CBVlad.Checked)
            {
                toDivision++;
            }

            if (Convert.ToDouble(TbValue.Text) > 0 && toDivision == 0)
            {
                MessageBox.Show("choose somebody");
                return;
            }

            double[] positivepositiveDept= new double[4];

            double[] negativeDept = new double[4];



            positivepositiveDept[0] = Convert.ToDouble(TbValue.Text) / toDivision;
            positivepositiveDept[1] = Convert.ToDouble(TbValue.Text) / toDivision;
            positivepositiveDept[2] = Convert.ToDouble(TbValue.Text) / toDivision;
            positivepositiveDept[3] = Convert.ToDouble(TbValue.Text) / toDivision;

            if (!CBPavel.Checked)
            {
                positivepositiveDept[0] = 0;
            }
            if (!CBMakara.Checked)
            {
                positivepositiveDept[1] = 0;
            }
            if (!CBKisil.Checked)
            {
                positivepositiveDept[2] = 0;
            }
            if (!CBVlad.Checked)
            {
                positivepositiveDept[3] = 0;
            }

            positivepositiveDept[0] += Convert.ToDouble(TbPavel.Text);
            positivepositiveDept[1] += Convert.ToDouble(TbMakara.Text);
            positivepositiveDept[2] += Convert.ToDouble(TbKisil.Text);
            positivepositiveDept[3] += Convert.ToDouble(TbVlad.Text);


            positivepositiveDept[0] = Convert.ToDouble(LbPavelDolg.Text) + positivepositiveDept[0];
            positivepositiveDept[1] = Convert.ToDouble(LbMakaraDolg.Text) + positivepositiveDept[1];
            positivepositiveDept[2] = Convert.ToDouble(LbKisilDolg.Text) + positivepositiveDept[2];
            positivepositiveDept[3] = Convert.ToDouble(LbVladDolg.Text) + positivepositiveDept[3];


            if (id == "3")
            {
                positivepositiveDept[0] = 0;
            }else if (id == "6")
            {
                positivepositiveDept[1] = 0;
            }else if( id == "4")
            {
                positivepositiveDept[2] = 0;
            }else if(id == "7")
            {
                positivepositiveDept[3] = 0;
            }

            negativeDept[0] = positivepositiveDept[0] * -1;
            negativeDept[1] = positivepositiveDept[1] * -1;
            negativeDept[2] = positivepositiveDept[2] * -1;
            negativeDept[3] = positivepositiveDept[3] * -1;



            DB_communicate dB_Communicate = new DB_communicate();

            dB_Communicate.SetData(positivepositiveDept, negativeDept, this.id);


            this.Hide();
            MainForm openForm = new MainForm(this.Left, this.Top, id);
            openForm.Show();

        }

        private void TbValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            bool used = false;
            used = TbValue.Text.Contains(",");
            char number = e.KeyChar;
            if (used)
            {
                if (!Char.IsDigit(number) && number != 8)
                {
                    e.Handled = true;
                }
            }
            else
            {
                if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && number != 44) 
                {
                    e.Handled = true;
                }
            }
            
        }

        private void TbPavel_KeyPress(object sender, KeyPressEventArgs e)
        {
            bool used = false;
            used = TbPavel.Text.Contains(",");
            char number = e.KeyChar;
            if (used)
            {
                if (!Char.IsDigit(number) && number != 8)
                {
                    e.Handled = true;
                }
            }
            else
            {
                if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && number != 44)
                {
                    e.Handled = true;
                }
            }
        }

        private void TbMakara_KeyPress(object sender, KeyPressEventArgs e)
        {
            bool used = false;
            used = TbMakara.Text.Contains(",");
            char number = e.KeyChar;
            if (used)
            {
                if (!Char.IsDigit(number) && number != 8)
                {
                    e.Handled = true;
                }
            }
            else
            {
                if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && number != 44)
                {
                    e.Handled = true;
                }
            }
        }

        private void TbKisil_KeyPress(object sender, KeyPressEventArgs e)
        {
            bool used = false;
            used = TbKisil.Text.Contains(",");
            char number = e.KeyChar;
            if (used)
            {
                if (!Char.IsDigit(number) && number != 8)
                {
                    e.Handled = true;
                }
            }
            else
            {
                if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && number != 44)
                {
                    e.Handled = true;
                }
            }
        }

        private void TbVlad_KeyPress(object sender, KeyPressEventArgs e)
        {
            bool used = false;
            used = TbVlad.Text.Contains(",");
            char number = e.KeyChar;
            if (used)
            {
                if (!Char.IsDigit(number) && number != 8)
                {
                    e.Handled = true;
                }
            }
            else
            {
                if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && number != 44)
                {
                    e.Handled = true;
                }
            }
        }

        private void CBSelectAll_CheckStateChanged(object sender, EventArgs e)
        {
            if (!CBSelectAll.Checked)
            {
                CBPavel.Checked = false;
                CBMakara.Checked = false;
                CBKisil.Checked = false;
                CBVlad.Checked = false;
            }
            else
            {
                CBPavel.Checked = true;
                CBMakara.Checked = true;
                CBKisil.Checked = true;
                CBVlad.Checked = true;
            }
        }

        private void TbValue_Enter(object sender, EventArgs e)
        {
            if (TbValue.Text == "0")
            {
                TbValue.Clear();
            }
        }

        private void TbValue_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TbValue.Text))
            {
                TbValue.Text = "0";
            }
        }

        private void TbPavel_Enter(object sender, EventArgs e)
        {
            if (TbPavel.Text == "0")
            {
                TbPavel.Clear();
            }
        }

        private void TbPavel_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TbPavel.Text))
            {
                TbPavel.Text = "0";
            }
        }

        private void TbMakara_Enter(object sender, EventArgs e)
        {
            if (TbMakara.Text == "0")
            {
                TbMakara.Clear();
            }
        }

        private void TbMakara_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TbMakara.Text))
            {
                TbMakara.Text = "0";
            }
        }

        private void TbKisil_Enter(object sender, EventArgs e)
        {
            if (TbKisil.Text == "0")
            {
                TbKisil.Clear();
            }
        }

        private void TbKisil_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TbKisil.Text))
            {
                TbKisil.Text = "0";
            }
        }

        private void TbVlad_Enter(object sender, EventArgs e)
        {
            if (TbVlad.Text == "0")
            {
                TbVlad.Clear();
            }
        }

        private void TbVlad_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TbVlad.Text))
            {
                TbVlad.Text = "0";
            }
        }

        private void CbCalc_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("calc");
        }
    }
}
